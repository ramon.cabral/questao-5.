//Não deu para fazer a área e perímetro do escaleno.

import java.util.*;
public class Main
{
public static void main(String[] args)
    {
    Scanner lado1=new Scanner(System.in);
    Double lado1_=lado1.nextDouble();
    Scanner lado2=new Scanner(System.in);
    Double lado2_=lado2.nextDouble();
    Scanner lado3=new Scanner(System.in);
    Double lado3_=lado3.nextDouble();
    //Se não for nem isósceles nem Equilátero o Boolean vai estar falso
    Boolean verificado=false;
    if(lado1_-lado2_==0 && lado2_-lado3_==0)
        {
        verificado=true;
        Double altura=(lado1_*Math.sqrt(3))/2;
        Double area=(lado1_*altura)/2;
        Double perimetro=lado1_*3;
        System.out.println("Área = "+Double.toString(area)+"; Perimetro ="+Double.toString(area)+" Tipo: Equilátero");
        }
    if((lado1_-lado2_==0 && lado1_-lado3_!=0) || (lado1_-lado3_==0 && lado2_-lado3_!=0) || (lado2_-lado3_==0 && lado1_-lado2_!=0))
        {
        verificado=true;
        Double perimetro=lado1_+lado2_+lado3_;
        Double [] lados = {lado1_,lado2_,lado3_};
        Double base = Double.NEGATIVE_INFINITY;
        for(int x=0; x<3; x++)
            {
            if(lados[x]>base)
                {
                base=lados[x];
                }
            }
        //Vou supor que a altura do triângulo = o valor de um lado diferente da base ao quadrado = altura ao quadrado + base/2 ao quadrado
        //Encontra o lado diferente da base para aplicar o teorema.
        Boolean encontrou=false;
        Double outroLado=0.0;
        if(base!=lado1_)
            {
            encontrou=true;
            outroLado=lado1_;
            }
        if(base!=lado2_ && encontrou==false)
            {
            encontrou=true;
            outroLado=lado2_;        
            }
        if(base!=lado3_ && encontrou==false)
            {
            encontrou=true;
            outroLado=lado3_;        
            }
            //Acho que fiz a fórmula certo.
        Double altura=Math.sqrt(Math.pow(outroLado,2)-Math.pow((base/2),2));
        //Área = (Base * altura)/2
        Double Area=(base*altura)/2;
        System.out.println("Perímetro = "+Double.toString(perimetro)+". Área = "+Double.toString(Area)+". Triangulo isósceles");
        }
    if(verificado==false) System.out.println("Triângulo escaleno");
    }
}